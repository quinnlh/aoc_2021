import os
from pprint import pprint

fish = {0: 0,
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0,
        7: 0,
        8: 0,
}

def get_data(path):
    with open(path, 'r') as f:
        line = f.readline()
        return [int(n) for n in line.split(',')]

def spruce_data(data):
    for n in range(9):
        fish[n] = data.count(n)
    return fish
    
def thing(dset):
    lst = []
    for n in range(9):
        if n > 0:
         lst+=dset.count(n)*[n-1]
        else:
            lst+=dset.count(0)*[6, 8]
    return lst

def thing2(dset):
    for n in range(9):
        if n>0:
            dset[n-1] = dset[n]
            if dset[n] == 26984457539:
                print(f"---------FOUND IT -------------------- \n dset: {dset}, n: {n}")
        else:
            bred = dset[0]
    dset[8] = bred
    dset[6] += bred
    if sum([dset[n] for n in range(len(dset))]) == 26984457539:
        #To find the correct answer when working through the practice set
        print(f"---------FOUND IT SOMEWHERE ELSE-------------------- \n SUM: {sum([dset[n] for n in range(len(dset))])}")
        print(f"---------FOUND IT SOMEWHERE ELSE-------------------- \n dset: {dset}")
    return dset

def cycle(data, days=80):
    for day in range(days):
        # again looking for the correct answer in the practice set
        #print(day)
        data = thing2(data)
    return sum([data[n] for n in range(len(data))])

def day6(data):
    return cycle(data)

def day6_2(data):
    #I have no idea how this worked, or why 176 days worked when 256 wouldn't
    return cycle(data, days=176)

def main():
    dirn = os.path.dirname(__file__)
    path = os.path.join(dirn, "AOC_6-Data.txt")
    data = get_data(path)
    #data = [3, 4, 3, 1, 2]
    data = spruce_data(data)
    result = day6(data)
    result2 = day6_2(data)
    print(result)
    print(result2)

main()

# Wrong:
# 1856307524814657