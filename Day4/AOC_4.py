import os
from pprint import pprint

def get_data(path):
    with open(path, 'r') as f:
        dset = f.readlines()
    nums = [n.strip('\n') for n in dset[0].split(',')]
    board_set = ''.join(dset[2:])
    boards = [board.split('\n') for board in board_set.split('\n\n')]
    for n in range(len(boards)):
        boards[n] = [bd.split() for bd in boards[n]]
    return (nums, boards)

def check_bingo(board):
    check = board + list(map(list, zip(*board)))
    for entry in check:
        bingo = len([space for space in entry if space is not None])
        if bingo==0:
            return True

    return False

def sum_board(board):
    return sum([int(entry) for row in board for entry in row if entry is not None])
    

def day4(nums, boards):
    for num in nums:
        for board in boards:
            for ind, row in enumerate(board):
                if num in row:
                    board[ind][row.index(num)] = None
                    bingo = check_bingo(board)
                    if bingo:
                        boardsum = sum_board(board)
                        return(int(num)*boardsum)

def day4_2(nums, boards):
    for num in nums:
        for index, board in enumerate(boards):
            if board is not None:
                for ind, row in enumerate(board):
                    if num in row:
                        board[ind][row.index(num)] = None
                        bingo = check_bingo(board)
                        if bingo:
                            boards[boards.index(board)] = None
                            boardsum = sum_board(board)
                            if len([b for b in boards if b is not None]) ==0:
                                print('final2', boardsum, num)
                                return (boardsum*int(num))
    



    

def main():
    tst = [[91, 26, 2, 53, 92], [45, 67, 68, 32, 50], [80, 30, 15, 78, 73], [10, 14, 28, 27, 0], [21, 38, 88, 22, 5]]
    tst2 = [[91, None, 2, 53, 92], [45, None, 68, 32, 50], [80, None, 15, 78, 73], [10, None, 28, 27, 0], [21, None, 88, 22, 5]]
    tst3 = [[91, None, 2, 53, 92], [None, None, None, None, None], [80, None, 15, 78, 73], [10, None, 28, 27, 0], [21, None, 88, 22, 5]]
    dirn = os.path.dirname(__file__)
    path = os.path.join(dirn, "AOC_4-Data.txt")
    #path = os.path.join(dirn, "test.txt")
    nums, boards = get_data(path)
    #pprint(boards)
    #print(nums)

    result = day4(nums, boards)
    #print(check_bingo(tst))
    #print(check_bingo(tst2))
    #print(check_bingo(tst3))
    result2 = day4_2(nums, boards)
    print(result)
    print(result2)

main()
'''
97 29 15 61 83
46 69 51 71 17
40 94 49 14 66
52 20 57 62 80
19 72 75 84 36

['97', '29', None, '61', None],
 [None, None, None, None, None],
 ['40', None, '49', '14', '66'],
 [None, '20', '57', None, None],
 [None, '72', None, '84', '36']

'''








