import os

def get_data(path):
    with open(path, 'r') as f:
        return [line.strip("\n").split() for line in f.readlines()]

def day1(commands):
    h = 0
    depth = 0
    for command in commands:
        if command[0] == "forward":
            h += int(command[1])
        elif command[0] == "up":
            depth += int(command[1])
        elif command[0] == "down":
            depth -= int(command[1])
    return h, depth

def day1_2(commands):
    aim = 0
    h = 0
    depth = 0
    for command in commands:
        if command[0] == "forward":
            h += int(command[1])
            depth += aim*int(command[1])
        elif command[0] == "up":
            aim -= int(command[1])
        elif command[0] == "down":
            aim += int(command[1])
    return h, depth

def main():
    dirn = os.path.dirname(__file__)
    path = os.path.join(dirn, "AOC_2-Data.txt")
    data = get_data(path)
    h, depth = day1(data)
    print(h*depth)
    h2, depth2 = day1_2(data)
    print(h2*depth2)

main()