import os

def get_data(path):
    with open(path, 'r') as f:
        return [int(line.strip("\n")) for line in f.readlines()]

def day1(data):
    count = 0
    return len([data[n] for n in range(len(data)) if n>0 and data[n]>data[n-1]])

def day1_2(data):
    grouped_data = [data[n]+data[n+1]+data[n+2] for n in range(len(data)-2)]
    return day1(grouped_data)

def main():
    dirn = os.path.dirname(__file__)
    path = os.path.join(dirn, "AOC_Data.txt")
    data = get_data(path)
    print(day1(data))
    print(day1_2(data))

main()