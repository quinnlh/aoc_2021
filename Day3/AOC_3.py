import os
import statistics

def get_data(path):
    with open(path, 'r') as f:
        return [line.strip("\n") for line in f.readlines()]

def common_bit(data, n):
    bitn = [d[n] for d in data]
    try:
        md = statistics.mode(bitn)
    except statistics.StatisticsError: #when there is no mode
        md = '1'
    return md

def bstr_to_int(nums):
    full = ''.join(nums)
    return int(full, 2)

def day3(data):
    mc = [] 
    for n in range(len(data[0])):
        mc.append(common_bit(data, n))
    lc = [str(0**int(n)) for n in mc]
    gamma = bstr_to_int(mc)
    epsilon = bstr_to_int(lc)
    return gamma*epsilon

def cycle(method, data, n):
    if method == 'most':
        test_n = common_bit(data, n)
    elif method=='least':
        test_n = str(0**int(common_bit(data, n)))
    return [row for row in data if row[n]==test_n]

def get_ogr(data):
    for n in range(len(data[0])):
        data = cycle('most', data, n)
    if len(data) > 2:
        get_ogr(data)
    return data[0]

def get_co2sr(data):
    for n in range(len(data[0])):
        data = cycle('least', data, n)
        if len(data)<2:
            break
    if len(data) > 2:
        get_ogr(data)
    return data[0]

def day3_2(data):
    ogr = bstr_to_int(get_ogr(data))
    co2sr = bstr_to_int(get_co2sr(data))
    return ogr*co2sr

def main():
    dirn = os.path.dirname(__file__)
    path = os.path.join(dirn, "AOC_3-Data.txt")
    data = get_data(path)
    result = day3(data)
    result2 = day3_2(data)
    print(result)
    print(result2)

main()