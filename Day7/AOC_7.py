import os
from pprint import pprint

def get_data(path):
    with open(path, 'r') as f:
        line = f.readline()
        return [int(n) for n in line.split(',')]

def calc_cost(value):
    derived = 0
    while value != 0:
        derived += value
        value -= 1
    return derived

def day7(data):
    trials = [sum([abs(num-n) for num in data]) for n in range(max(data))]
    return min(trials)

def day7_2(data):
    trials = [sum([calc_cost(abs(num-n)) for num in data]) for n in range(max(data))]
    return 

def main():
    dirn = os.path.dirname(__file__)
    path = os.path.join(dirn, "AOC_7-Data.txt")
    data = get_data(path)
    result = day7(data)
    result2 = day7_2(data)
    print(result)
    print(result2)

main()
